<div align="center">
<h1>default_value_cj</h1>
</div>

## <img alt="" src="./doc/readme-image/readme-icon-introduction.png" style="display: inline-block;" width=3%/> 1 介绍

### 1.1 项目特性

  * 实现仓颉的数据类型的默认值，用来解决仓颉数组Array<T>在使用泛型作为类别时，repeat无法设置的问题
  * 目前对全部基本数据类型和String都进行了扩展，自定义类型可以使用继承Defaultable接口或者使用@defaultBuilder宏的方式

#### 一、Defaultable 默认值接口

 * 静态属性：static prop default 

#### 二、@defaultBuilder 宏

 * 类宏
 * 自动添加Defaultable接口实现及其default属性:
 
    返回值：
    
    * 有主构造函数：使用参数默认值的主构造函数
    * 无主构造函数：生成一个可变变量的全参主构造函数


## <img alt="" src="./doc/readme-image/readme-icon-framework.png" style="display: inline-block;" width=3%/> 2 架构

### 2.1 项目结构

```shell
.
├── README.md
├── LICENSE
├── cjpm.toml
├── doc
|   └── readme-image
|
└── src
    ├── macros
    |   └── default_builder.cj
    ├── test
    |   └── default_test.cj
    ├── util
    |   └── util.cj
    ├── defaultable.cj
    └── extends.cj
        
```

## <img alt="" src="./doc/readme-image/readme-icon-compile.png" style="display: inline-block;" width=3%/> 3 使用说明

### 3.1 编译构建（Win/Linux/Mac）

```shell
cjpm build
```

### 3.2 功能示例

#### 3.2.1 基本数据类型
```cangjie
import default_value_cj.*

@TestCase
func genericTypeTest() {
    @Assert(Int64.default,0)
    @Assert(Int32.default,0)
    @Assert(Int16.default,0)
    @Assert(Int8.default,0)
    @Assert(Float16.default,0.0)
    @Assert(Float32.default,0.0)
    @Assert(Float64.default,0.0)
    @Assert(Bool.default,false)
    @Assert(Rune.default,r'\0')
    @Assert(String.default,"")
}
```

#### 3.2.2 无成员变量类
```cangjie
import default_value_cj.*
import default_value_cj.macros.*

@defaultBuilder
public class NoParamClassMacro <: Equatable<NoParamClassMacro> {
    public operator func ==(that: NoParamClassMacro): Bool {
        return refEq(this, that)
    }
}

@TestCase
func customizedTypeTest(){
   // 无变量的类 -- 使用宏实现defaultable接口
   @Assert(NoParamClassMacro() == NoParamClassMacro.default,false)

}
```

#### 3.2.3 只有基本变量类型的可变变量的类
```cangjie
import default_value_cj.*
import default_value_cj.macros.*

@defaultBuilder
public class HaveOnlyVarParamClassMacro <: Equatable<HaveOnlyVarParamClassMacro> {
    var a: Int64 = 0
    var b: Float64 = 0.0
    var c: Bool
    var d: Rune

    public operator func ==(that: HaveOnlyVarParamClassMacro): Bool {
        if (this.a != that.a || this.b != that.b || this.c != that.c || this.d != that.d) {
            return false
        }
        return true
    }
}

@TestCase
func customizedTypeTest(){
   // 只有primative var变量的类 -- 使用宏实现defaultable接口
   @Assert(HaveOnlyVarParamClassMacro(Int64.default,Float64.default,Bool.default,Rune.default) == HaveOnlyVarParamClassMacro.default)
}
```
#### 3.2.4 泛型默认值功能使用
```cangjie
import default_value_cj.*
import default_value_cj.macros.*

public class ArrayList<T> where T <:Defaultable<T>{
    private var _collection : Array<T>
    public init(capacity: Int64) {
        // 可以正常编译通过
        _collection = Array<T>(capacity, repeat: T.default)
    }
}

@defaultBuilder
class User <: Equatable<User>{
    private var id : Int64
    private var score :Float64
    public operator func ==(that:User):Bool{
        return this.id == that.id && this.score == that.score
    }
    
}

main():Unit{
    // 可以正常编译通过
    let arr1 = ArrayList<Int64>(10)
    let arr2 = ArrayList<User>(10)
}
```

## <img alt="" src="./doc/readme-image/readme-icon-contribute.png" style="display: inline-block;" width=3%/> 4 参与贡献

本项目由 [SIGCANGJIE / 仓颉兴趣组](https://gitcode.com/SIGCANGJIE) 实现并维护。技术支持和意见反馈请提Issue。

本项目基于 Apache License 2.0，欢迎给我们提交PR，欢迎参与任何形式的贡献。

本项目committer：[@Chemxy](https://gitcode.com/Chemxy)

This project is supervised by [@zhangyin-gitcode](https://gitcode.com/zhangyin_gitcode).